# Masscan on Docker

Run the latest version of masscan with Docker and Docker Compose.

It gives you the ability to analyze any data set by using the searching/aggregation capabilities of Elasticsearch and
the visualization power of Kibana.

Based on the official Docker images from Elastic (also thanks to deviantony for the great work)

* [Elasticsearch](https://github.com/elastic/elasticsearch/tree/master/distribution/docker)
* [Logstash](https://github.com/elastic/logstash/tree/master/docker)
* [Kibana](https://github.com/elastic/kibana/tree/master/src/dev/build/tasks/os_packages/docker_generator)

## Contents
1. [Requirements](#requirements)
   * [Host setup](#host-setup)
   * [SELinux](#selinux)
   * [Docker for Desktop](#docker-for-desktop)
     * [Windows](#windows)
     * [macOS](#macos)
2. [Usage](#usage)
   * [Bringing up the stack](#bringing-up-the-stack)
   * [Cleanup](#cleanup)
   * [Initial setup](#initial-setup)
3. [Masscan](#masscan)
   * [How to configure masscan](#masscan)


## Requirements

### Host setup

* [Docker Engine](https://docs.docker.com/install/) version **19.03.1**
* [Docker Compose](https://docs.docker.com/compose/install/) version **1.24.1**
* 8 GB of RAM

By default, the stack exposes the following ports:
* 5000: Logstash TCP input
* 9200: Elasticsearch HTTP
* 9300: Elasticsearch TCP transport
* 5601: Kibana
* nil : Masscan

### SELinux

On distributions which have SELinux enabled out-of-the-box you will need to either re-context the files or set SELinux
into Permissive mode in order for docker-elk to start properly. For example on Redhat and CentOS, the following will
apply the proper context:

```console
$ chcon -R system_u:object_r:admin_home_t:s0 docker-elk/
```

### Docker for Desktop

#### Windows

Ensure the [Shared Drives][win-shareddrives] feature is enabled for the `C:` drive.

#### macOS

The default Docker for Mac configuration allows mounting files from `/Users/`, `/Volumes/`, `/private/`, and `/tmp`
exclusively. Make sure the repository is cloned in one of those locations or follow the instructions from the
[documentation][mac-mounts] to add more locations.

## Usage

### Bringing up the stack

Clone this repository, then start the stack using Docker Compose:

```console
$ docker-compose up
```

You can also run all services in the background (detached mode) by adding the `-d` flag to the above command.
If you are starting the stack for the very first time, please read the section below attentively.

## Initial setup

#### On the command line

Create an index template via the Kibana API:

```console
curl -XPUT http://elastic:changeme@localhost:9200/_template/scanresults -H 'Content-Type: application/json' -d '
{
  "index_patterns": ["scan-*"],
  "settings": {
    "number_of_shards": 1
  },
  "mappings": {
    "_source": {
      "enabled": false
    },
    "properties": {
        "@timestamp": {
          "type": "date",
          "format": "dateOptionalTime"
        },
        "@version": {
          "type": "integer"
        },
        "time_t": {
          "type": "date",
          "format": "strict_date_optional_time||epoch_millis"
        },
        "ip": {
          "type": "ip"
        },
        "ip_proto": {
          "type": "keyword"
        },
        "port": {
          "type": "integer"
        },
        "service": {
          "type": "keyword"
        },
        "state": {
          "type": "keyword"
        },
        "banner": {
          "type": "text"
        },

        "geoip": {
          "type": "object",
          "dynamic": true,
          "properties": {
            "location": {
              "type": "geo_point"
            }
          }
        }
    }
  }
}'
```

## Masscan

Masscan data are persisted into a volume, which is shared with logstash

To configure the scan parameter edit the .env file:

```console
CIDR_BLOCK=192.168.2.0/24
PORTS=0-65535
RATE=10
```

### Cleanup

Elasticsearch data is persisted inside a volume by default.

In order to entirely shutdown the stack and remove all persisted data, use the following Docker Compose command:

```console
$ docker-compose down -v
```

